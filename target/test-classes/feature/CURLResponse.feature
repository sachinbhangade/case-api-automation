Feature: Test CURL response

Scenario: CURL actual response and expected response should match
When tester send CURL request
Then CURL response should be as we expect

Scenario: authStatusCurrent is not sended in CURL request
When tester send curl request without sending authStatusCurrent field
Then authStatusCurrent should be N/A in CURL response

Scenario: authStatusCurrent sended as V in CURL request
When authStatusCurrent sended as V in CURL request
Then authStatusCurrent should be V in CURL response

@RunThis
Scenario: questionTag is missing in CURL request
When questionTag has not sended in CURL request
Then there should not be any error 

