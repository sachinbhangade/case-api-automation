package com.qpid.automation.exception;

public class InvalidEnvironmentException extends Exception{
	public InvalidEnvironmentException(String errorMessage){
		super(errorMessage);
	}
	
}
