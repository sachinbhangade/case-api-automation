package com.qpid.automation.base.constant;

public class Constants {

	/**
	 * Declare Constant field here
	 */
	private Constants() {

	}

	public static final String LOGFILE_PATH = System.getProperty("user.dir") + "\\executioninfo\\logs\\"; 
	public static final int minValue = 000000001;
	public static final int maxValue = 999999999;
	

}
