package com.qpid.automation.base.property;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.qpid.automation.base.logger.Logg;

public class PropertyManager {
	private PropertyManager(){		
	}
	private static final Logger LOGGER = Logg.createLogger();
	protected static final Properties prop = new Properties();
	public static final String APPLICATIONPROPERTIESPATH = "/src/test/resources/properties/";
	
	public static Properties loadApplicationProperty(){
		try{
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
					+APPLICATIONPROPERTIESPATH + "application.properties");
			prop.load(fis);
		}catch(IOException io){
			LOGGER.error(io);
			
		}
		catch(Exception e){
			LOGGER.error(e);			
		}
		
		return prop;
		
	}

}
