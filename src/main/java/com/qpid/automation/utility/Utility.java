package com.qpid.automation.utility;

import java.io.File;
import java.util.Hashtable;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import com.qpid.automation.base.constant.Constants;
import com.qpid.automation.base.logger.Logg;

public class Utility {
	private static final Logger LOGGER = Logg.createLogger();

	

	public static Logger initLog(String msg) {

		FileAppender appender = new FileAppender();
		// appender.setFile(System.getProperty("user.dir")+"\\executioninfo\\logs\\"+msg+".log");
		appender.setFile(Constants.LOGFILE_PATH + msg + ".log");
		appender.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
		appender.setAppend(false);
		appender.activateOptions();
		Logger APPLICATION_LOG = Logger.getLogger(msg);
		APPLICATION_LOG.setLevel(Level.DEBUG);
		APPLICATION_LOG.addAppender(appender);
		return APPLICATION_LOG;
	}

	public static String getCurrentThreadId() {
		return "Thread:" + Thread.currentThread().getId() + "	-";
	}

	/**
	 * @param dirName
	 *            is the name of the folder that's to be created
	 * @return boolean based on the whether the file is created or not
	 */
	public static boolean createDirIfNotExists(String dirName) {
		File dir = new File(dirName);
		boolean result = false;
		// if the directory does not exist, create it
		if (!dir.exists()) {
			LOGGER.debug("Creating Logs directory : " + dir);
			result = false;
			try {
				dir.mkdir();
				result = true;
			} catch (SecurityException se) {
				LOGGER.error(Utility.getCurrentThreadId() + "SecurityException " + se.getStackTrace(), se);
			}
			if (result) {
				LOGGER.debug(dirName + " dir created");
			}
		} else
			LOGGER.debug(dirName + "folder already exists");
		return result;
	}

}
