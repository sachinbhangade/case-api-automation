package com.qpid.automation.utility;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import org.apache.log4j.Logger;
import com.qpid.automation.base.constant.Constants;
import com.qpid.automation.base.logger.Logg;
import com.qpid.automation.base.property.PropertyManager;
import com.qpid.automation.exception.InvalidEnvironmentException;

import cucumber.api.Scenario;

public class CommonMethods 
{
	private static final Logger LOGGER = Logg.createLogger();
	public static Properties APPLICATIONPROPERTY = PropertyManager.loadApplicationProperty();
	static String path = System.getProperty("user.dir")+"//src//test/resources//testData//TestData.xlsx";
	static Xls_Reader xls = new Xls_Reader(path);
	public static Map<String,String> scenarioTestCaseDataMap;
	public static String curlFinalRequest;
	public static String curlExpectedResponse;
	public static final String ENVIRONMENT = APPLICATIONPROPERTY.getProperty("environment", "Local");
	
	//Generating Random number which will be used as CASE ID
	public static int generateRandomNumber() throws IllegalArgumentException{
		Random r = new Random();
		int randomNUmber = r.nextInt((Constants.maxValue - Constants.minValue) + 1) + Constants.minValue;
		return randomNUmber;
	}
	
	//Reading data from Excel into Map and returning Map variable
	public static Map<String, String> getTestCaseData(Scenario scenario) {
		for(int i = 1; i<xls.getRowCount("Scenario");i++){
			if(xls.getCellData("Scenario","Scenario Name",(i+1)).equalsIgnoreCase(scenario.getName())){
				String testCaseName = xls.getCellData("Scenario", "TestID", (i+1));
				LOGGER.info("Test Case Name is : "+testCaseName);
				
				//Take scenario data into Map
				scenarioTestCaseDataMap = new HashMap<String,String>(); 
				int totalTestCaseRow = xls.getRowCount(testCaseName);
				int totalTestCaseColumn = xls.getColumnCount(testCaseName);						
				for(int dataRow = 1; dataRow<totalTestCaseRow;dataRow++){
					for(int datCol=0;datCol<totalTestCaseColumn;datCol++){
						String colName = xls.getCellData(testCaseName, datCol, 1);
						String rowData = xls.getCellData(testCaseName, datCol, dataRow+1);
						scenarioTestCaseDataMap.put(colName, rowData);					
					}
				}				
			}
		}
		LOGGER.info(scenarioTestCaseDataMap);
		return scenarioTestCaseDataMap;
	}

		
	/**
	 Generate random number for CASE ID
	 If environment is Integration then Tester should provide CASE ID
	 If environment is QA Local then we will generate random number
	 * @throws InvalidEnvironmentException 
	 */
	public void getCurlInput(String curlInput1, String curlInput2, String curlOutput1, String curlOutput2) throws InvalidEnvironmentException {
		if(ENVIRONMENT.equalsIgnoreCase("Local")){
		int randomNumber = generateRandomNumber();
		LOGGER.info("CASE ID generated randomly is: "+randomNumber);
		curlFinalRequest = curlInput1+randomNumber+curlInput2;
		curlExpectedResponse = curlOutput1+randomNumber+curlOutput2;
		LOGGER.info("CURL Request is : "+curlFinalRequest);
		LOGGER.info("CURL Expected Response is : "+curlExpectedResponse);
		}
		else if(ENVIRONMENT.equalsIgnoreCase("Integration")){
			String case_ID = scenarioTestCaseDataMap.get("CaseID");
			LOGGER.info("CASE ID used is : "+case_ID);
			curlFinalRequest = curlInput1+case_ID+curlInput2;
			curlExpectedResponse = curlOutput1+case_ID+curlOutput2;
			LOGGER.info("CURL Request is : "+curlFinalRequest);
			LOGGER.info("CURL Expected Response is : "+curlExpectedResponse);
		}
		else{
			throw new InvalidEnvironmentException("environement variable should be either Local or Integration");
		}
	}

}
