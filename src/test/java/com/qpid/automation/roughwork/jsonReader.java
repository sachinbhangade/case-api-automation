package com.qpid.automation.roughwork;
/*package com.qpid.automation.resdJSON;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

public class jsonReader {

	*//**
	 * This code I will be using in final implementation, below is the source link
	 * http://www.journaldev.com/2315/java-json-example
	 * @param JSON_FILE
	 * @return
	 * @throws IOException
	 * <!-- <dependency>
			<groupId>org.glassfish</groupId>
			<artifactId>javax.json</artifactId>
			<version>1.0.4</version>
		</dependency> -->
	 *//*
	public static boolean testReadingJson(String JSON_FILE) throws IOException {
		String jsonFile = JSON_FILE;
		InputStream fis = new ByteArrayInputStream(jsonFile.getBytes());

		// Create JSON Reader
		JsonReader jsonReader = Json.createReader(fis);

		// get JsonObject from JsonReader
		JsonObject jsonObject = jsonReader.readObject();

		// we can close IO resource and JsonReader now
		jsonReader.close();
		fis.close();

		JsonValue approvalRecommendation = jsonObject.get("approvalRecommendation");
		// jsonObject.getString("approvalRecommendation");
		JsonValue expectedFalsePositiveRatio = jsonObject.get("expectedFalsePositiveRatio");
		System.out.println("approvalRecommendation " + approvalRecommendation);
		System.out.println("expectedFalsePositiveRatio is " + expectedFalsePositiveRatio);

		if (approvalRecommendation.toString() == "true") {
			if (expectedFalsePositiveRatio.toString() != "null") {
				System.out.println("mission accomplished");
				return true;
			}
			else{
				System.out.println("mission unsuccessful");
				return false;
			}
		} else
			return true;
	}
}
*/