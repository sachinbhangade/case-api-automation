package com.qpid.automation.roughwork;

import java.util.Random;
import java.util.stream.LongStream;



/**
 * @author sachinb6
 * This program generate random number which will be used as CaseID
 *
 */
public class RandomNumberGenerator {
	
	public void generateRandomNumber(int min, int max){
		if (min >= max) {
	        throw new IllegalArgumentException("max must be greater than min");
	    }

	    Random r = new Random();
	    int randomNUmber =  r.nextInt((max - min) + 1) + min;
	    System.out.println(randomNUmber);
	}
	

	public static void main(String[] args) {
		RandomNumberGenerator test = new RandomNumberGenerator();
		System.out.println("***********************");
		test.generateRandomNumber(000000001,999999999);
		System.out.println("***********************");	
	}

}
