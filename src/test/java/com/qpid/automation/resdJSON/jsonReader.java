package com.qpid.automation.resdJSON;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.qpid.automation.base.logger.Logg;

/**
 * @author sachinb6 Source :
 * Read the JSON file using JSON Simple library       
 */
public class jsonReader {
	private static final Logger LOGGER = Logg.createLogger();

	//Read the JSON file
	public static JSONObject jsonParser(String jsonInput) throws ParseException {
		String json_Input = jsonInput;
		JSONParser jsonParser = new JSONParser();
		Object object = jsonParser.parse(json_Input);
		JSONObject jsonObject = (JSONObject) object;
		return jsonObject;
	}

	//Return true if approvalRecommendationField is true
	public static boolean readExpectedFalsePositiveRatioField(String jsonInput) {
		boolean approvalRecommendationField = false;
		String expectedFalsePositiveRatioField = null;
		try {
			expectedFalsePositiveRatioField = (String) jsonParser(jsonInput).get("expectedFalsePositiveRatio");
			LOGGER.debug("expectedFalsePositiveRatioField value is " + expectedFalsePositiveRatioField);
			approvalRecommendationField = (boolean) jsonParser(jsonInput).get("approvalRecommendation");
			LOGGER.debug("approvalRecommendationField value is " + approvalRecommendationField);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (approvalRecommendationField) {
			if (expectedFalsePositiveRatioField.toString() != "null") {
				LOGGER.debug("approvalRecommendationField file value is : "+expectedFalsePositiveRatioField);
				return true;
			} else {
				LOGGER.error("approvalRecommendationField file value is : "+expectedFalsePositiveRatioField+" not as expected");
				return false;
			}
		} else
			return true;
	}

	public static boolean readExpectedNegativePredictiveValue(String jsonInput) {

		boolean nonCertRecommendation = false;
		String expectedNegativePredictiveValue = null;
		try {
			expectedNegativePredictiveValue = (String) jsonParser(jsonInput).get("expectedFalsePositiveRatio");
			LOGGER.info("expectedFalsePositiveRatioField value is " + expectedNegativePredictiveValue);
			nonCertRecommendation = (boolean) jsonParser(jsonInput).get("approvalRecommendation");
			LOGGER.info("approvalRecommendationField value is " + nonCertRecommendation);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (nonCertRecommendation) {
			if (expectedNegativePredictiveValue.toString() != "null") {
				LOGGER.debug("approvalRecommendationField file value is : "+expectedNegativePredictiveValue);
				return true;
			} else {
				LOGGER.error("approvalRecommendationField file value is : "+expectedNegativePredictiveValue+" not as expected");
				return false;
			}
		} else
			return true;
	
	}
}
