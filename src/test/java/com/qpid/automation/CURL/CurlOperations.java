package com.qpid.automation.CURL;

import static com.jayway.restassured.RestAssured.given;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.skyscreamer.jsonassert.FieldComparisonFailure;
import org.skyscreamer.jsonassert.JSONCompare;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.JSONCompareResult;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.response.Response;
import com.qpid.automation.base.logger.Logg;
import com.qpid.automation.base.property.PropertyManager;

public class CurlOperations {
	private static final Logger LOGGER = Logg.createLogger();
	public static Properties APPLICATIONPROPERTY = PropertyManager.loadApplicationProperty();
	public static final String CURLURL = APPLICATIONPROPERTY.getProperty("curlURL");

	public String sendCurlRequest(String curlRequest) {
		RestAssured.baseURI = CURLURL;
		Response r = (Response) given().
									relaxedHTTPSValidation().
									contentType("application/json").
									body(curlRequest)
								.when().post("");
		return r.getBody().asString();
	}      
	
	public boolean verifyResponse(String curlExpectedResponse, String curlActualResponse) throws JSONException {
		JSONCompareResult result = JSONCompare.compareJSON(curlExpectedResponse, curlActualResponse,
				JSONCompareMode.LENIENT);
		LOGGER.info(result.getFieldFailures().size());
		if (result.getFieldFailures().size() == 0) {
			return true;
		}
		else if (result.getFieldFailures().size() == 1) {
			for (FieldComparisonFailure x : result.getFieldFailures()) {
				LOGGER.info(x.getField() + " and " + x.getActual());
				if ((x.getField().equals("metadata.predictionProbability"))) {
					return true;
				}
			}
		} else {
			LOGGER.error("Response mismatch for two or more keys");
			for (FieldComparisonFailure x : result.getFieldFailures()) {
				LOGGER.info("Field name is " + x.getField() + " actual value is  " + x.getActual()
						+ " expected value is " + x.getExpected());
			}
			return false;
		}
		return false;
	}

	public boolean verifyResponseWhenAuthStatusCurrentNotSent(String curlExpectedResponse, String curlActualResponse)
			throws JSONException {
		boolean testCaseExecutionResultFlag = false;
		boolean authStatusResultFlag = false;
		JSONCompareResult result = JSONCompare.compareJSON(curlExpectedResponse, curlActualResponse,
				JSONCompareMode.LENIENT);
		LOGGER.info(result.getFieldFailures().size());

		if (result.getFieldFailures().size() == 2) {
			for (FieldComparisonFailure x : result.getFieldFailures()) {
				if ((x.getField().equals("metadata.authStatusCurrent") && x.getActual().equals("N/A"))) {
					LOGGER.info(x.getField() + " and " + x.getActual());
					authStatusResultFlag = true;
				} else if (x.getField().equals("metadata.predictionProbability")) {
					LOGGER.info(x.getField() + " and " + x.getActual());
					testCaseExecutionResultFlag = true;
				}
			}
		} else {
			testCaseExecutionResultFlag = false;
		}
		if (testCaseExecutionResultFlag && authStatusResultFlag) {
			return true;
		} else if (!authStatusResultFlag) {
			LOGGER.error("Response mismatch for metadata.authStatusCurrent keys");
			return false;
		} else {
			LOGGER.error("Response mismatch for more than two keys");
			return false;
		}
	}

	public static boolean verifyCurlResponse(String curlExpectedResponse, String curlActualResponse)
			throws JSONException {
		JSONCompareResult result = JSONCompare.compareJSON(curlExpectedResponse, curlActualResponse,JSONCompareMode.LENIENT);
		LOGGER.info(result.getFieldFailures().size());
		if (result.getFieldFailures().size() == 0) {
			return true;
		} else if (result.getFieldFailures().size() == 1) {
			for (FieldComparisonFailure x : result.getFieldFailures()) {
				LOGGER.info(x.getField() + " and " + x.getActual());
				if ((x.getField().equals("metadata.predictionProbability"))) {
					return true;
				}
			}
		} else {
			LOGGER.info("Response mismatch for two or more keys");
			for (FieldComparisonFailure x : result.getFieldFailures()) {
				LOGGER.info("Field name is " + x.getField() + " actual value is  " + x.getActual()
						+ " expected value is " + x.getExpected());
			}
			return false;
		}
		return false;
	}
}
