package com.qpid.automation.feature;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.testng.Assert;
import com.qpid.automation.CURL.CurlOperations;
import com.qpid.automation.base.logger.Logg;
import com.qpid.automation.resdJSON.jsonReader;
import com.qpid.automation.utility.CommonMethods;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author sachinb6 This is CURL Step definition class
 *
 */
public class CURLResponseStepsDef {

	private static final Logger LOGGER = Logg.createLogger();
	CurlOperations curlOperations = new CurlOperations();
	String curlActualResponse;
	public int caseID;

	@When("^tester send CURL request (\\d+)$")
	public void tester_send_CURL_request(int repetition) {
		try {
			String curlInputRequest = CommonMethods.curlFinalRequest;
			curlActualResponse = curlOperations.sendCurlRequest(curlInputRequest);
			LOGGER.info("CURL Actual Response is : " + curlActualResponse);
		} catch (NullPointerException e) {
			LOGGER.error("send CURL request failed", e);
		}

	}

	@Then("^CURL response should be as we expect$")
	public void curl_response_should_be_as_we_expect() throws Throwable {
		try {
			Assert.assertTrue(curlOperations.verifyResponse(CommonMethods.curlExpectedResponse, curlActualResponse));
			LOGGER.info("CURL response is as we expect");
		} catch (JSONException e) {
			LOGGER.error("Step CURL response should be as we expect is failed", e);
		}
	}

	@When("^tester send curl request without sending authStatusCurrent field$")
	public void tester_send_curl_request_without_sending_authStatusCurrent_field() {
		String curlInputRequest = CommonMethods.curlFinalRequest;
		curlActualResponse = curlOperations.sendCurlRequest(curlInputRequest);
		LOGGER.info("CURL Actual Response is : " + curlActualResponse);
	}

	@Then("^authStatusCurrent should be N/A in CURL response$")
	public void curl_authStatusCurrent_should_be_N_A() {
		try {
			Assert.assertTrue(curlOperations.verifyResponseWhenAuthStatusCurrentNotSent(
					CommonMethods.curlExpectedResponse, curlActualResponse));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@When("^authStatusCurrent sended as V in CURL request$")
	public void authstatuscurrent_sended_as_V_in_CURL_input() {
		String curlInputRequest = CommonMethods.curlFinalRequest;
		
		curlActualResponse = curlOperations.sendCurlRequest(curlInputRequest);
		LOGGER.info("CURL Actual Response is : " + curlActualResponse);
	}

	@Then("^authStatusCurrent should be V in CURL response$")
	public void authstatuscurrent_should_be_V_in_CURL_response() {

		try {
			
			Assert.assertTrue(
					CurlOperations.verifyCurlResponse(CommonMethods.curlExpectedResponse, curlActualResponse));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@When("^questionTag has not sended in CURL request$")
	public void questiontag_has_not_sended_in_CURL_request() throws Throwable {
		String curlInputRequest = CommonMethods.curlFinalRequest;
		// LOGGER.info("CURL Input Request is : "+curlInputRequest);
		curlActualResponse = curlOperations.sendCurlRequest(curlInputRequest);
		LOGGER.info("CURL Actual Response is : " + curlActualResponse);
	}

	@Then("^there should not be any error$")
	public void there_should_not_be_any_error() throws Throwable {
		try {
			Assert.assertTrue(
					CurlOperations.verifyCurlResponse(CommonMethods.curlExpectedResponse, curlActualResponse));
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@When("^authStatusCurrent is A or D$")
	public void authstatuscurrent_is_A_or_D() throws Throwable {
		try {
			String curlInputRequest = CommonMethods.curlFinalRequest;
			curlActualResponse = curlOperations.sendCurlRequest(curlInputRequest);
			LOGGER.info("CURL Actual Response is : " + curlActualResponse);
		} catch (Exception e) {
			LOGGER.error("Step authStatusCurrent is A or D failed", e);
		}
	}

	@Then("^audit field should populate as False$")
	public void audit_field_should_populate_as_False() throws Throwable {
		try {

			Assert.assertTrue(
					CurlOperations.verifyCurlResponse(CommonMethods.curlExpectedResponse, curlActualResponse));
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	@When("^curl response contain approvalRecommendation as true$")
	public void curl_response_contain_approvalRecommendation_as_true() throws Throwable {
		String curlInputRequest = CommonMethods.curlFinalRequest;
		curlActualResponse = curlOperations.sendCurlRequest(curlInputRequest);
		LOGGER.info("CURL Actual Response is : " + curlActualResponse);
	}

	@Then("^expectedFalsePositiveRatio field should be non null$")
	public void expectedfalsepositiveratio_field_should_be_non_null() throws Throwable {
		Assert.assertTrue(jsonReader.readExpectedFalsePositiveRatioField(curlActualResponse), "expectedFalsePositiveRatio is not as expected");
	}
	
	@When("^curl response contain nonCertRecommendation as true$")
	public void curl_response_contain_nonCertRecommendation_as_true() throws Throwable {
		String curlInputRequest = CommonMethods.curlFinalRequest;
		curlActualResponse = curlOperations.sendCurlRequest(curlInputRequest);
		LOGGER.info("CURL Actual Response is : " + curlActualResponse);
	}

	@Then("^expectedNegativePredictiveValue field should be non null$")
	public void expectednegativepredictivevalue_field_should_be_non_null() throws Throwable {
		Assert.assertTrue(jsonReader.readExpectedNegativePredictiveValue(curlActualResponse), "expectedFalsePositiveRatio is not as expected");
	}
	
	@When("^Test$")
	public void test()  {
	   System.out.println("Test");
	   //int test = 10/0;
	  // System.out.println(test);
	}

	@Then("^test1$")
	public void test1()  {
		System.out.println("Test1");
	}
}
