package com.qpid.automation.feature;

import java.util.Map;

import org.apache.log4j.Logger;

import com.qpid.automation.base.logger.Logg;
import com.qpid.automation.exception.InvalidEnvironmentException;
import com.qpid.automation.utility.CommonMethods;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class hooks {

	private static final Logger LOGGER = Logg.createLogger();
	private Scenario scenario;
	public static Map<String, String> scenarioDataMap;
	CommonMethods commonMethodObject = new CommonMethods();

	@Before
	public void setup(Scenario scenario)   {
		this.scenario = scenario;
		try{		
		LOGGER.info("Environment is "+CommonMethods.ENVIRONMENT);
		LOGGER.info("Scenario name is : " + scenario.getName());
		scenarioDataMap = CommonMethods.getTestCaseData(scenario);		
		if(!(scenarioDataMap == null)){
		commonMethodObject.getCurlInput(scenarioDataMap.get("CurlInputRequestPart1"),
										scenarioDataMap.get("CurlInputRequestPart2"), 
										scenarioDataMap.get("CurlOutputResponsePart1"),
										scenarioDataMap.get("CurlOutputResponsePart2"));
		}else{
			LOGGER.warn("There is no data for scenario "+scenario.getName());
		}
		}catch(InvalidEnvironmentException e){
			LOGGER.error(e);
		}
	}

	@After
	public void tesrDown() {
		scenarioDataMap = null;
		System.out.println("This is After tag");
		System.out.println("***************************");
	}

}
