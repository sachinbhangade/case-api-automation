Feature: Test CURL response

  @RunThis @Smoke
  Scenario Outline: CURL actual response and expected response should match
    When tester send CURL request <Repeat>
    Then CURL response should be as we expect

    Examples: 
      | Repeat |
      |      1 |
      |      2 |
      |      3 |
      |      4 |
      |      5 |
      |      6 |
      |      7 |
      |      8 |
      |      9 |
      |     10 |

  @RunThis
  Scenario: authStatusCurrent is not sent in CURL request
    When tester send curl request without sending authStatusCurrent field
    Then authStatusCurrent should be N/A in CURL response

  @RunThis
  Scenario: authStatusCurrent sent as V in CURL request
    When authStatusCurrent sended as V in CURL request
    Then authStatusCurrent should be V in CURL response

  @RunThis
  Scenario: questionTag is missing in CURL request
    When questionTag has not sended in CURL request
    Then there should not be any error

  @RunThis
  Scenario: If authStatusCurrent is A or D, audit should always be False
    When authStatusCurrent is A or D
    Then audit field should populate as False

  @RunThis
  Scenario: when approvalRecommendation is true then expectedFalsePositiveRatio should be non-null
    When curl response contain approvalRecommendation as true
    Then expectedFalsePositiveRatio field should be non null

  @RunThis
  Scenario: when nonCertRecommendation is true then expectedNegativePredictiveValue should be non-null
    When curl response contain nonCertRecommendation as true
    Then expectedNegativePredictiveValue field should be non null
